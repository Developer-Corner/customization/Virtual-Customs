
# Virtual Customs
 Portable Desktop Client for https://virtualcustoms.net/

## Requirements

 * [nativefier](https://github.com/nativefier/nativefier)

<img width="100%" src="res/vc-logo.png">

## Instructions

### Windows

 * Clone the repo into any location of your choosing.
 * Double click the `build.cmd` script to build the Client and wait for it to finish.
 * Locate the `Virtual Customs.exe` file inside the newly created folder and Double Click it to run the app.

### Linux

 * Clone the repo into any location of your choosing.
 * Run the `build.sh` file in terminal to build the Client and wait for it to finish.
 * CD into the newly created folder.
 * Run the  `Virtual Customs` file in terminal to run the Client

<details>

 ## Features

 * **Background Support**: Close button minimizes the app to the system tray, allowing it to continue running in the background *(To exit, simply right click the system tray icon and click exit)*.
 * **Portable App**: All data is stored inside the app, making it completely portable and capable of being stored on a flash drive.
 * **Internal URLs**: Internal URL support built in for anything on the HBO Max namespace *(This includes the login screen)*.
 * **Simple Updates**: App utilizes a build script to make buidling and updating easier for the user *(requires [nativefier](https://github.com/nativefier/nativefier))*.

## Screenshot(s)

<img src="res/screenshot(1).png">

</details>
